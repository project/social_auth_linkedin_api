<?php

namespace Drupal\social_auth_linkedin_api\Controller;

use Drupal\social_auth_decoupled\SocialAuthDecoupledTrait;
use Drupal\social_auth_decoupled\SocialAuthHttpInterface;
use Drupal\social_auth_linkedin\Controller\LinkedInAuthController;

/**
 * Post login responses for Social Auth linkedin.
 */
class LinkedinAuthHttpController extends LinkedInAuthController implements SocialAuthHttpInterface {

  use SocialAuthDecoupledTrait;

  /**
   * {@inheritdoc}
   */
  public function authenticateUserByProfile($profile, $data) {
    $name = $profile->getFirstName() . ' ' . $profile->getLastName();
    $email = $this->providerManager->getEmail();
    return $this->userAuthenticatorHttp()
      ->authenticateUser($name, $email, $profile->getId(), $this->providerManager->getAccessToken(), $profile->getImageUrl(), $data);
  }

}
